var ddsportmodule = angular.module('starter.services')

.factory('WebApi', ['$q', '$rootScope', function ($q, $rootScope, $http) {
  return {

    getAll: function (tn) {

      var url = $rootScope.BaseUrl + tn;
      var deferred = $q.defer();
      var filter = "";
      $http({
          method: 'GET',
          url: url
        })
        .then(function (response) {
          deferred.resolve.apply(this, response);
        }, function () {
          deferred.reject.apply(this, response);
        });
      return deferred.promise; 
    }
  }
}]);