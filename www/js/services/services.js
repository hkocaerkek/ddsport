var ddsportmodule = angular.module('starter.services', ['azure'])

.factory('Db', ['client', '$q', '$rootScope', '$http', '$rootScope', function (client, $q, ApiEndpoint, $http, $rootScope) {

$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
  // added for jason date format fix.
  var iso8601RegEx = /(19|20|21)\d\d([-/.])(0[1-9]|1[012])\2(0[1-9]|[12][0-9]|3[01])T(\d\d)([:/.])(\d\d)([:/.])(\d\d)/;

  function fnConverDate(input) {
    if (typeof input !== "object") return input;

    for (var key in input) {
      if (!input.hasOwnProperty(key)) continue;

      var value = input[key];
      var type = typeof value;
      var match;
      if (type == 'string' && (match = value.match(iso8601RegEx))) {
        input[key] = new Date(value)
      } else if (type === "object") {
        fnConverDate(value);
      }
    }
  }
  // jason format fix

  return {

    getAll: function (tn) {
      var url = $rootScope.BaseUrl + tn;
      return $http.get(url)
        .then(function (response) {
          console.log('Got some data: ', response.data);
          fnConverDate(response.data);
          console.log('after convert data: ', response.data);
          return response.data;
        });
    },

    get: function (tn, id) {
      var url = $rootScope.BaseUrl + tn + "/" + id;
      return $http.get(url)
        .then(function (response) {
          console.log('Got some data: ', response.data);
          fnConverDate(response.data);
          console.log('after convert data: ', response.data);
          return response.data;
        });
    },

    getChilds: function (tn, id, tn2) {
      var url = $rootScope.BaseUrl + tn + "/" + id + "/" + tn2;
      return $http.get(url)
        .then(function (data) {
          console.log('Got some data: ', data.data);
          return data.data;
        });
    },

    getWithFilter: function (tn, id, filter, value) {
      var url = $rootScope.BaseUrl + tn + "/" + id + '?' + filter + '=' + value;
      return $http.get(url)
        .then(function (data) {
          console.log('Got some data: ', data.data);
          return data.data;
        });
    },

    deleteWithId: function (tn, instance) {
      var deferred = $q.defer();
      client.getTable(tn).del(instance).then(function () {
        deferred.resolve.apply(this, arguments);
      }, function () {
        deferred.reject.apply(this, arguments);
      });
      return deferred.promise;
    },

    // PUT: api/teachers/5
    // POST: api/teachers

    add: function (tn, obj) {
      var deferred = $q.defer();
 
      // add
      if (obj.id == null) {
         
        var url = $rootScope.BaseUrl +  tn;
        return $http.post(url,obj, {
                                    headers : {'Content-Type' : 'application/json'   
                                      }
                                  })
          .then(function (response) {
            console.log('Got some data: ', response.data);
            fnConverDate(response.data);
            console.log('after convert data: ', response.data);
            return response.data;
          });
      } else // update
      {
        delete obj.updatedAt;
        delete obj.createdAt;
        delete obj.deleted;

        var url = $rootScope.BaseUrl + tn + '/' + obj.id;
        return $http.put(url,obj)
          .then(function (response) {
            console.log('Got some data: ', response.data);
            fnConverDate(response.data);
            console.log('after convert data: ', response.data);
            return response.data;
          });
      }
      return deferred.promise;
    },

  };
}])

;
