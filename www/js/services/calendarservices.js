var ddsportmodule = angular.module('starter.services')

.factory('Calendar', ['$q', '$rootScope', function ($q, $rootScope) {
  var usehalfhour = true;
  return {
    getWeekDays: function () {
      return [{
        id: 0,
        name: "Monday"
      }, {
        id: 1,
        name: "Tuesday"
      }, {
        id: 2,
        name: "Wednesday"
      }, {
        id: 3,
        name: "Thursday"
      }, {
        id: 4,
        name: "Friday"
      }, {
        id: 5,
        name: "Saturday"
      }, {
        id: 6,
        name: "Sunday"
      }, ];;
    },

    getMonthNames: function () {
      var monthname = new Array(12);
      monthname[0] = "January";
      monthname[1] = "February";
      monthname[2] = "March";
      monthname[3] = "April";
      monthname[4] = "May";
      monthname[5] = "June";
      monthname[6] = "July";
      monthname[7] = "August";
      monthname[8] = "September";
      monthname[9] = "October";
      monthname[10] = "November";
      monthname[11] = "December";
      return monthname;
    },

    t: function (hour, minute) {
      return moment().hour(hour).minute(minute);
    },

    getHours: function (startHour, endHour, _usehalfhour) {
      this.usehalfhour = _usehalfhour;
      var tmp = [];
      for (i = startHour; i <= endHour; i++) {
        tmp.push(('0' + i).slice(-2) + ':00');
        if (_usehalfhour && i < endHour) {
          tmp.push(('0' + i).slice(-2) + ':30');
        }
      }

      return tmp;
    },

    getDays: function () {
      var tmp = [];
      var date1 = new Date();
      var date2 = new Date();
      date2.setDate(date2.getDate() + 1);

      var weekday = this.getWeekDays();
      var monthname = this.getMonthNames();

      tmp.push({
        day: weekday[date1.getDay()].id,
        longdate: weekday[date1.getDay()].name + ', ' + monthname[date1.getMonth()] + ' ' + date1.getDate() + ', ' + date1.getFullYear(),
        datevalue: date1,
        dateformat: date1.toLocaleDateString()
      });
      //  tmp.push({ day: weekday[date2.getDay()], longdate: weekday[date2.getDay()] + ', ' + monthname[date2.getMonth()] + ' ' + date2.getDate() + ', ' + date2.getFullYear(), datevalue: date2, dateformat: date2.toLocaleDateString() });
      console.log(tmp);
      return tmp;
    },

    getRooms: function () {
      var tmp = [];
      tmp.push({
        id: 1,
        name: 'Ana Salon'
      });
      tmp.push({
        id: 2,
        name: 'Zemin 1'
      });
      tmp.push({
        id: 3,
        name: 'Zemin 2'
      });
      tmp.push({
        id: 4,
        name: 'Birinci Kat 1'
      });
      tmp.push({
        id: 5,
        name: 'Birinci Kat 1'
      });
      tmp.push({
        id: 6,
        name: 'Birinci Kat 2'
      });
      tmp.push({
        id: 7,
        name: 'Yan bina 1'
      });
      return tmp;
    },

    createEvent: function (classid, name, roomOrder, start, end, eventtype, color, date) {
      var base = moment().hour(7).minute(0);
      var durH = end.diff(start, 'hours');
      var durM = end.diff(start, 'minutes');
      var durX = durM / 60;
      var durS = start.diff(base, 'minutes') / 60;

      _left = 60 + roomOrder * 120
       if( !this.usehalfhour) durS = durS /2;
      _top = 23 + durS * 100
      if( !this.usehalfhour) durX = durX /2;
      _height = durX * 100;

      var event = {
        classid: classid,
        eventname: name,
        starthour: '08:00',
        endhour: '09:30',
        eventtype: eventtype,
        room: 'room ' + roomOrder,
        left: _left + 'px',
        top: _top + 'px',
        height: _height + 'px',
        color: color,
        date: new Date(),
        dateformat: new Date().toLocaleDateString()
      };
      return event;
    },


  };
}])

;