angular.module('starter.controllers')

.controller('DocumentModalCtrl', function($scope, $ionicPopup, Db, $state, $stateParams, $rootScope, $http, $ionicHistory, $cordovaCamera) {

    $scope.documentImage = "";
    $scope.documentType = "";

    $scope.takePicture = function() {

        if (typeof Camera == 'undefined') {
            $scope.documentImage = $scope.documents[0].image;
            return;
        }

        var options = {
            quality: 100,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.CAMERA,
            allowEdit: true,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 300,
            targetHeight: 300,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false
        };

        $cordovaCamera.getPicture(options).then(function(imageData) {
            $scope.documentImage = "data:image/jpeg;base64," + imageData;
        }, function(err) {
            // An error occured. Show a message to the user
            $scope.documentImage = $scope.documents[0];
        });
    }

    $scope.saveDocument = function(item, image, documentType) {
        console.log(item);
        item.documentType = documentType; //  {id : 1, name : "kimlik" };
        item.teacher = $scope.item;
        item.name = "test";
        item.image = $scope.documentImage;
        console.log(item);

        Db.add("documents", item).then(function(params) {

                if (item.id == null) { // if this is an insert, clear form.
                } else {
                    alert("document " + item.id + " updated.");
                }
            },
            function(error) {
                console.log(error);
                //alert(error.data.Message);
            });
    }

})