var weekdays = Calendar.getWeekDays(); 

var monthname = Calendar.getMonthNames();

function t(hour, minute) {
    return moment().hour(hour).minute(minute);
}

function getHours(startHour, endHour, usehalfhour) {
    var tmp = [];
    for (i = startHour; i <= endHour; i++) {
        tmp.push(('0' + i).slice(-2) + ':00');
        if (usehalfhour && i < endHour) {
            tmp.push(('0' + i).slice(-2) + ':30');
        }
    }

    return tmp;
};

function getRooms() {
    var tmp = [];
    tmp.push({
        id: 1,
        name: 'Ana Salon'
    });
    tmp.push({
        id: 2,
        name: 'Zemin 1'
    });
    tmp.push({
        id: 3,
        name: 'Zemin 2'
    });
    tmp.push({
        id: 4,
        name: 'Birinci Kat 1'
    });
    tmp.push({
        id: 5,
        name: 'Birinci Kat 1'
    });
    tmp.push({
        id: 6,
        name: 'Birinci Kat 2'
    });
    tmp.push({
        id: 7,
        name: 'Yan bina 1'
    });
    return tmp;
};

function createEvent(name, roomOrder, start, end, eventtype, color, date) {
    var base = moment().hour(7).minute(0);
    var durH = end.diff(start, 'hours');
    var durM = end.diff(start, 'minutes');
    var durX = durM / 60;
    var durS = start.diff(base, 'minutes') / 60;

    _left = 60 + roomOrder * 120
    _top = 23 + durS * 100
    _height = durX * 100;

    var event = {
        eventname: name,
        starthour: '08:00',
        endhour: '09:30',
        eventtype: eventtype,
        room: 'room ' + roomOrder,
        left: _left + 'px',
        top: _top + 'px',
        height: _height + 'px',
        color: color,
        dateformat: new Date().toLocaleDateString()
    };
    return event;
}