angular.module('starter.controllers')
    .controller('CalendarCtrl', function ($scope, $ionicScrollDelegate, Calendar) {

 var usehalfhour = false;
        $scope.timerleft = '0px';
        $scope.hours = Calendar.getHours(7, 21, false);
        $scope.rooms = Calendar.getRooms();
        $scope.days = Calendar.getDays();
        $scope.events = getEvents();

        $scope.gotScrolled = function () {
            $scope.timerleft = $ionicScrollDelegate.getScrollPosition().left + 'px';
            $scope.$apply();
        };

        function getEvents() {
            var tmp = [];
            var date1 = new Date();
            t = Calendar.t;

            tmp.push(Calendar.createEvent('','Ders 1', 0, t(8, 00), t(9, 45), 'ion-mic-c', 'blue', Date()));
            tmp.push(Calendar.createEvent('','Ara', 0, t(9, 45), t(10, 00), 'ion-coffee', 'brown', Date()));
            tmp.push(Calendar.createEvent('','Sohpet', 0, t(11, 00), t(11, 50), 'ion-chatbubbles', 'salmon', Date()));
            tmp.push(Calendar.createEvent('','Öğle Yemeği', 0, t(12, 30), t(14, 00), 'ion-wineglass', 'blue', Date()));
            tmp.push(Calendar.createEvent('','Akşam Yemeği', 0, t(19, 00), t(21, 00), 'ion-wineglass', 'green', Date()));
            //
            //
            //Presentation - 0,157,151 -- ion-mic-c
            //Networking 18,67,172 -- ion-chatbubbles
            //Coffee Break 255,169,0, --ion-coffee
            //Dinner 255,113,0 --ion-wineglass
            return tmp;
        };

    });