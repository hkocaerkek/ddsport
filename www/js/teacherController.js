angular.module('starter.controllers')

.controller('TeacherCtrl', function($scope, $ionicPopup, Db, $state, $stateParams, $rootScope, $http, $ionicHistory, $cordovaCamera, $ionicModal) {

    $rootScope.schoolid = 1;
    $scope.tableName = 'teachers';
    $scope.shouldShowDelete = false;
    $scope.shouldShowReorder = false;
    $scope.listCanSwipe = true;
    $scope.documentType = {};
    $scope.showOzluk = true;
    $scope.showDocs = true;
    $scope.showSalary = true;
    $scope.documentImage = "";

    $ionicModal.fromTemplateUrl('templates/addDocument.html', {
        scope: $scope,
        animation: 'slide-in-up',
        backdropClickToClose: true,
        hardwareBackButtonClose: true,
        focusFirstInput: true
    }).then(function(modal) {
        $scope.modal = modal;
        console.log($scope.modal);
    });

    $scope.openModal = function() {
        $scope.document = {
            "teacher": $scope.item
        };
        $scope.modal.show();
    };
    $scope.closeModal = function() {
        $scope.modal.hide();
    };
    // Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function() {
        $scope.modal.remove();
    });
    // Execute action on hide modal
    $scope.$on('modal.hidden', function() {
        // Execute action
    });
    // Execute action on remove modal
    $scope.$on('modal.removed', function() {
        // Execute action
    });

    $scope.takePicture = function() {
        var options = {
            quality: 75,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.CAMERA,
            allowEdit: true,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 300,
            targetHeight: 300,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false
        };

        $cordovaCamera.getPicture(options).then(function(imageData) {
            $scope.item.image = "data:image/jpeg;base64," + imageData;
        }, function(err) {
            // An error occured. Show a message to the user
        });
    }

    function getDocumentTypes() {
        Db.getAll("documenttypes").then(function(result) {
                    $scope.documentTypes = result;
                },
                function(error) {
                    alert(error);
                    $scope.documentTypes = [];
                })
            .finally(function() {

            })
    };

    function getDocuments() {
        Db.getAll("documents").then(function(result) {
                    $scope.documents = result;
                },
                function(error) {
                    alert(error);
                    $scope.documents = [];
                })
            .finally(function() {

            })
    };

    function getItem(id) {
        Db.get($scope.tableName, id).then(function(result) {
                    $scope.item = result;
                },
                function(error) {
                    alert(error);
                })
            .finally(function() {
                // Stop the ion-refresher from spinning
                $scope.$broadcast('scroll.refreshComplete');
            })
    };

    function getItems() {
        Db.getAll($scope.tableName).then(function(result) {
            $scope.items = result;
        }, function(error) {
            alert(error);
        }).finally(function() {
            // Stop the ion-refresher from spinning
            $scope.$broadcast('scroll.refreshComplete');
        })
    };

    $scope.doRefresh = function() {
        getItems();

    };

    // students must belongs to a school, 
    // students can belongs to a class
    $scope.addItem = function(item) {
        // add student to the school
        if ($rootScope.schoolid == null) {
            alert("You must add student to a school");
            return;
        }
        item.schoolid = $rootScope.schoolid;
        Db.add($scope.tableName, item).then(function(params) {

                if (item.id == null) { // if this is an insert, clear form.
                    // alert("student " + item.name + " added.");
                    //getItems();

                    // if classid != null we must add this student to classstudent table.
                    if ($rootScope.classid != null) {
                        var classteacher = {
                            classid: $rootScope.classid,
                            teacherid: params.id
                        };
                        Db.add('classteacher', classteacher).then(function(params) {
                            // then return to class page
                            $ionicHistory.goBack(-2);
                            $state.go('app.class', {
                                Id: $rootScope.classid
                            }, {
                                reload: true,
                                location: 'replace'
                            });
                        });
                    }
                    //
                    //$ionicHistory.nextViewOptions({disableBack: true});
                    //$window.location = "/#/app/students" ;// + params.id;
                } else {
                    alert("student " + item.name + " updated.");
                }
            },
            function(error) {
                alert(error.data.Message);
            });
    }

    $scope.deleteItem = function(item) {

        var tablename = 'teacher';

        // ask for delete
        confirmPopup.then(function(res) {
            if (res) {
                // first get the instance
                Db.get(tablename, item.id).then(function(result) {
                        var obj = result;
                        // then delete the instance
                        Db.deleteWithId(tablename, obj).
                        then(function(result) {
                                // get items.
                                getItems();
                            },
                            function(error) {
                                alert(error)
                            }
                        )

                    },
                    function(error) {
                        alert(error)
                    })
            }
        })
    };


    if ($stateParams.Id) {
        getItem($stateParams.Id);
        getDocuments();
        getDocumentTypes();
    } else {
        getItems();
    }


})